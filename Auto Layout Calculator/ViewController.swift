//
//  ViewController.swift
//  Auto Layout Calculator
//
//  Created by Alek Ivanovski on 2/19/19.
//  Copyright © 2019 Alek Ivanovski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var resultLabel: UILabel!
    
    var currentResult: Double = 0
    var operand1: Double = 0
    var operand2: Double = 0
    var operation: Int = 0
    var resultBeingShown = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        resultLabel.text = "0"
        
    }
    
    
    @IBAction func clearResultPressed(_ sender: Any) {
        operand1 = 0
        operand2 = 0
        currentResult = 0
        operation = 0
        resultBeingShown = false
        resultLabel.text = "0"
    }
    
    
    @IBAction func equalsPressed(_ sender: Any) {
        if operation != 0 {
            populateOperand2()
            doMath(operation: self.operation)
            operation = 0
            resultLabel.text = String(currentResult)
            resultBeingShown = true
        }
    }
    
    @IBAction func numberPressed(_ sender: UIButton) {
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 6
        
        if resultBeingShown == true {
            resultLabel.text = "0"
            resultBeingShown = false
        }
        
        if resultLabel.text == "0" {
            resultLabel.text = String(sender.tag)
        } else {
            let newNumber = stringNumberToDouble(numberAsText: resultLabel.text! + String(sender.tag))
            resultLabel.text = numberFormatter.string(from: NSNumber(value: Double(newNumber)))
        }

    }
    
    @IBAction func floatinfPointPressed(_ sender: Any) {
        let currentNumber: String = resultLabel.text!
        var newNumber: String = ""
        
        if !currentNumber.contains(".") {
            newNumber = currentNumber + "."
        } else {
            newNumber = currentNumber
        }
        
        resultLabel.text = newNumber
    }
    
    @IBAction func operationButtonPressed(_ sender: UIButton) {
        
        if operand1 == 0 {
            populateOperand1()
            resultLabel.text = "0"
            self.operation = sender.tag
        } else if resultBeingShown == false {
            populateOperand2()
            self.operation = sender.tag
            doMath(operation: sender.tag)
            resultLabel.text = String(currentResult)
            resultBeingShown = true
        } else {
            self.operation = sender.tag
            doMath(operation: sender.tag)
            resultLabel.text = String(currentResult)
            resultBeingShown = true
        }
        
    }
    
    func doMath(operation: Int) {
        switch operation {
        case 10:
//            self.operation = 10
            add()
        case 11:
//            self.operation = 11
            subtract()
        case 12:
//            self.operation = 12
            multiply()
        case 13:
//            self.operation = 13
            devide()
        case 14:
//            self.operation = 14
            percent()
        case 15:
//            self.operation = 15
            changeSign()
        default:
            print("Error operation")
        }
    }
    
    func add() {
        currentResult = operand1 + operand2
        operand1 = currentResult
        operand2 = 0
    }
    
    func subtract() {
        currentResult = operand1 - operand2
        operand1 = currentResult
        operand2 = 0
    }
    
    func multiply() {
        currentResult = operand1 * operand2
        operand1 = currentResult
        operand2 = 0
    }
    
    func devide() {
        if operand2 != 0 {
            currentResult = operand1 / operand2
            operand1 = currentResult
            operand2 = 0
        }
    }
    
    func percent() {
        currentResult = operand1 * (operand2 / 100)
        operand1 = currentResult
        operand2 = 0
    }
    
    func changeSign() {
        currentResult = operand1 * (-1)
        operand1 = currentResult
    }
    
    func clearOperands() {
        operand1 = 0
        operand2 = 0
    }

    func populateOperand1() {
        self.operand1 = stringNumberToDouble(numberAsText: resultLabel.text!)
    }
    
    func populateOperand2() {
        self.operand2 = stringNumberToDouble(numberAsText: resultLabel.text!)
    }
    
    func stringNumberToDouble(numberAsText: String) -> Double {
        let numberAsTextWithoutCommas = numberAsText.replacingOccurrences(of: ",", with: "")

        return Double(numberAsTextWithoutCommas)!
    }
    
}

